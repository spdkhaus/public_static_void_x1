Funktionen mit Parametern 

 

def funktions_name(aufgelistete_parameter): 

    # Anweisungen 

 

Bei der Funktionsdefinition können in Klammern mit Kommata getrennt Parameter mitgegeben werden 

Diese werden beim Funktionsaufruf durch tatsächliche Werte, die sogenannten Argumente, ersetzt 

Der Funktionsaufruf muss die gleiche Anzahl an Argumenten und die gleiche Reihenfolge der Argumente verwenden, wie es in der Funktionsdefinition angegeben ist 

 