
Heute wollen wir das aus den Achtzigerjahren bekannte Computerspiel Snake programmieren.
Dabei läuft eine Schlange über den Bildschirm und versucht, ihr Essen einzufangen, in diesem Fall also den roten Kreis.
Immer wenn sie ein Essen gegessen hat, wird sie länger, das Ziel ist also, so lange wie möglich zu werden.
Dabei kann man aber auch verlieren, nämlich genau dann, wenn man über den Spielfeldrand hinausläuft oder eben, wenn die Schlange in sich selber läuft.
Das heißt also, wenn der schwarze Kopf in den gelben Körper läuft.
Wenn man sich das ansieht, sieht es auf den ersten Blick sehr schwer aus.
Doch wir werden das Stück für Stück programmieren, und dann ist das Ganze auch sehr machbar.
Dafür werden wir uns Stück für Stück kleine Bausteine definieren, die wir am Ende dann zusammensetzen wollen.
Das heißt also, ihr habt immer nach den Lektionen kleine Aufgaben zu dem Snake-Spiel und programmiert quasi immer Teile von dem Gesamtspiel.
Deshalb gibt es auch nach den Lektionen keine Selbsttests.
Wir starten also zunächst, indem wir das erste Element für unser Spiel erstellen.
Dazu möchten wir den Schlangenkopf durch ein schwarzes Quadrat repräsentieren und verwenden wieder die bekannte turtle-Bibliothek für Grafiken.
Wir beginnen gleich damit, den Stift in Zeile 6 hochzunehmen, damit sichergestellt ist, dass, sobald sich unser Schlangenkopf bewegt, wir keine Linie hinter uns her zeichnen.
Das Ganze sieht dann also so aus: Wir haben hier im Folgenden zu einigen Erklärungen ein Koordinatensystem hinzugefügt, damit wir besser auf die einzelnen Bewegungsschritte eingehen können.
Dieses Koordinatensystem sieht also so aus, und unser Koordinatensystem selbst geht dabei wieder von -200 bis +200 in jeder Ausrichtung.
Gucken wir uns das Ganze noch mal genauer an.
Wir sehen also, wir befinden uns genau in der Mitte, das heißt der Mittelpunkt von unserem schwarzen Quadrat hat die Koordinate 0, 0.
Zur Seite, jeweils nach links und rechts, können wir also 10 Schritte laufen, um wirklich auf die Kante von dem nächsten Nachbarfeld zu kommen.
Das heißt also, wenn wir uns nun ein Feld nach oben bewegen wollen, also auf das Feld laufen wollen, wo gerade das graue Quadrat ist,
müssen wir die y-Koordinate verändern, wenn wir dann also wissen wollen, was für eine Koordinate das graue Quadrat hat.
Also schauen wir nochmal, wie genau sehen dann die Koordinaten von dem grauen Viereck aus?
Auf der x-Achse verändert sich nichts, also auch, dass graue Quadrat ist wie das schwarze links und rechts von 10 und -10 zehn umgeben und das bleibt auch so.
Aber auf der y-Achse ändert sich was, denn wir laufen ja auf der y-Achse nach oben.
Das heißt also, unsere Koordinate ändert sich von 0 auf 20, und somit ist dann unsere abschließende Koordinate für das graue Quadrat 0 und 20.
Laufen ist dabei ein gutes Stichwort, schauen wir uns noch einmal an, wie die Bewegung funktioniert.
Wenn wir so wie eben einfach nur mit dem schwarzen Quadrat in der Mitte starten und dieses nach 0, 20 bewegen, dann stellen wir in der Vergrößerung fest,
dass wir eine kontinuierliche Bewegung haben, wie also wirklich vollständig das Raster durchschreiten.
Für die bekannte Optik aus dem Snake-Klassiker ist es aber er gewünscht, dass wir von einem Feld in das nächste Feld quasi springen,
also, dass es eine schnelle Bewegung gibt und diese eben nicht in Einzelschritten sichtbar ist.
Dazu gibt es die Funktion speed, diese nimmt einen Wert zwischen 0 und 10 entgegen, wobei 0 für die schnellstmögliche Animation steht.
Und wenn wir mit speed 0 vor dem goto-Aufruf die Geschwindigkeit setzen, dann stellen wir fest, dass das Quadrat tatsächlich von einem Feld ins nächste springt und wir diesen Zwischenschritt nicht mehr sehen.

Jetzt haben wir also gesehen, unseren Schlangenkopf haben wir bisher folgendermaßen definiert, wir haben also, wie wir schon gesagt haben, ein shape, eine Farbe festgelegt,
haben den Stift angehoben, bevor wir an eine Stelle gelaufen sind und haben also zuvor darauf geachtet, dass speed auch noch gesetzt wurde.
Nun wollen wir aber noch eine weitere Sache hinzufügen, nämlich eine Richtung für den Kopf.
Dafür gibt es in Turtle die sogenannte direction, die wir jetzt in dem Fall erst mal auf stop gesetzt haben.
Was wir damit später machen, erklären euch dann zum gegebenen Zeitpunkt, erst mal setzen wir sie auf stop, das ist, wie gesagt, schon so vordefiniert, wir gehen später darauf ein, was wir damit machen.
Wir wollen sie quasi später abfragen, aber das werdet ihr dann sehen.
Genau, das heißt, bisher haben wir also dieses kleine schwarze Quadrat definiert, sehen das in der Mitte und haben somit auch schon unseren ersten Baustein definiert, nämlich den mit den verschiedenen Grafiken.
Und da sehen wir jetzt also, da ist schon mal unser Schlangenkopf vorhanden.
Als nächstes brauchen wir natürlich noch das einzelne Essen, das wir eben mit dem Schlangenkopf einsammeln können, damit wir eben auch länger werden und so das Spiel eben auch Spaß macht.
Dazu verwenden wir einen roten Kreis beispielsweise und möchten den wieder genauso erzeugen, stellen also wieder sicher,
dass wir uns schnell bewegen, den Stift hochnehmen und setzen diesen jetzt an die Stelle 0, 100.
Wenn wir das Ganze aber nun einzeln ausführen und uns die Zwischenergebnisse mal anschauen, zum Beispiel eben nach Zeile 13,
dann haben wir zwar den roten Punkt erzeugt und bewegen diesen mit Zeile 16 auch nach oben.
Wir stellen aber fest, dass unser schwarzes Quadrat, also unser Schlangenkopf, nicht mehr vorhanden ist.
Das liegt daran, dass alle Funktionsaufrufe, die wie hier eben schreiben, auf demselben Bildelement ausgeführt werden.
 Das heißt, wir haben entweder ein schwarzes Quadrat oder eben den roten Kreis, und das ist für unser Spiel doch jetzt arg unpraktisch.
 Daher brauchen wir also die Möglichkeit, dass wir zwei Formen darstellen können.
Genau. Wir brauchen also mehrere Turtles, dafür gibt es in Turtle die Funktion Turtle.
Dabei müssen wir darauf achten, dass das T groß geschrieben wird, wir sehen das Ganze in Zeile 3 und Zeile 11.
Wir erstellen also mit dem Aufruf von dieser Funktion jetzt zwei verschiedene Turtles, einmal den kopf und einmal das essen.
Dabei können wir dann das, was wir da aufgerufen haben, einfach wirklich den verschiedenen Variablennamen zuweisen
und können dann ganz normal weiterhin mit der Turtle alles Mögliche machen und verschiedene Eigenschaften setzen wie schon zuvor.
Wir müssen nur eben darauf achten, dass wir jetzt kommt man durch eine Punktnotation die verschiedenen Aufrufe eben von der Variable trennen.
Wir sehen, also wir brauchen wirklich für jede Turtle, die wir in unserem Spiel haben wollen, eine eigene Variable.
Wir sehen also auch, wir haben dann am Ende unser Problem gelöst, jetzt können wir wirklich unseren Schlangenkopf und auch das Essen sehen und mit beiden verschieden dann im Spiel agieren.
Und somit haben wir zu unserem ersten Baustein auch schon das zweite kleine Stück hinzugefügt, nämlich nun die Grafik fürs Essen.
Schauen wir uns nun einmal an, wie die Steuerung funktioniert, denn schließlich wollen wir mit dem Schlangenkopf nun ja zum Essen kommen, damit wir eben wachsen können.
Wir verwenden dafür grüne Dreiecke, die wir in die untere rechte Bildecke bewegen möchten, damit wir darüber die Steuerung abbilden.
Das Erstellen von einem grünen Dreieck ist dabei recht simpel.
Wir haben es hier mal für das rechte Beispiel gemacht, aber die Positionierung ist eben nicht ganz so einfach.
Unser Ziel ist es also, die nach rechts unten zu bewegen, aber schauen wir uns doch diese Stelle einmal näher an.
Genau, wenn da jetzt auch wieder reinzoomen, sehen wir also, dass wir unten rechts wieder die Koordinate 200, -200 haben.
Wollen wir uns jetzt mal anschauen, wie genau das Dreieck für die Rechtsbewegung definiert ist.
Hier sehen wir also, dass wir auf der x-Achse von der 170 und 190 umgeben sind, deswegen hat das grüne Dreieck nach rechts die x-Koordinate 180,
und genauso können wir dann auch feststellen, dass die y-Koordinate von diesem Dreieck -160 ist.
Das Ganze können wir dann mit dem goto-Befehl also unserem Code hinzufügen, das sehen wir jetzt hier in Zeile 8, und genau so können wir dann auch für das untere Dreieck vorangehen.
Wichtig ist hierbei, dass wir noch eine Drehung ergänzen, denn wir wollen ja, dass das Dreieck mit der Spitze immer in genau die Richtung zeigt, die später für die Steuerung verantwortlich ist.
Daher drehen wir das Dreieck nach unten, also um 90 Grad nach rechts, und genau das tun wir hier, damit es dann entsprechend unten erscheint.
Wir brauchen, wie ihr schon gemerkt habt, hier auch keine Richtung anzugeben, denn diese ist ja nur für den Schlangenkopf notwendig.
Damit haben wir den Baustein um das dritte Element ergänzt, nämlich um die Einführung der Steuerungselemente.
Genau, jetzt wollen wir auch sehen, ob auf eins der Dreiecke geklickt wurde und wenn ja, eben auf welches genau,
und dafür definieren wir uns die Funktion interpretiere_eingabe, die die beiden Koordinaten x und y entgegennimmt.
Wir wollen also sehen, ob die Koordinaten von dem Mausklick wirklich mit einem der Mittelpunkte unserer Dreiecke übereinstimmt, die wir ja gerade definiert haben und die wir so auch kennen.
Schauen wir uns dazu also zuallererst die Bewegung nach unten wieder an, da sehen wir also, wo das Dreieck positioniert ist.
Wir sehen hier also auch, dass das Dreieck in dem Bereich von 150 bis 170 liegt. und genau diesen Bereich wollen wir jetzt auch abfangen.
Wir wollen also dem Nutzer ermöglichen, dass er nicht genau den Mittelpunkt eben treffen muss vor dem grünen Dreieck,
sondern den gesamten Bereich, in dem das Dreieck sich befindet, wenn wir jetzt wieder unser Zellenmuster, unser Raster anschauen.
Genau, also fragen wir hier eben x &gt;= 150 und x &lt;= 170 ab.
Genauso läuft das Ganze auch für y und eben nur, wenn alle Bedingungen wahr sind, darum hier auch die und-Verknüpfung, dann wollen wir die Richtung uns bewegen.
In dem Fall wollen wir dann die Funktion nach_unten_ausrichten aufrufen, was es damit auf sich hat, da kommen wir auch gleich zu.
Genau dasselbe machen auch für das Dreieck nach rechts, da sehen wir auch hier wieder die Koordinaten, die wir dafür definiert haben,
09:54und genau so ergeben sich daraus die verschiedenen Bedingungen eben für diese elif-Bedingung,
und genauso können wir dann auch an der Stelle eben eine bestimmte Funktion aufrufen, auf die wir gleich eingehen werden.
Und dann folgen eben noch weitere elif-Bedingungen für die verschiedenen anderen Dreiecke.
Am Ende von alldem wollen wir natürlich immer eben die Funktion aufrufen mit den richtigen Koordinaten, wenn auf den Bildschirm geklickt wird.
Dafür gibt es die Funktion onclick, der übergeben wir jetzt also eine Funktion.
Das ist ziemlich neu, das haben wir so noch nicht gemacht, aber das sieht dann halt so folgendermaßen aus.
Dabei müsst ihr also darauf achten, dass ihr nicht irgendwie die runden Klammern oder x und y noch extra onclick mit übergebt, sondern ihr schreibt wirklich nur den Funktionsnamen rein,
und dann übergibt onclick automatisch beim Klick auf den Bildschirm die x- und y-Koordinate eben unserer Funktion interpretiere_eingabe.
Genau. Und somit haben wir dann auch unseren letzten oder nächsten Baustein definiert, nämlich onclick (interpretiere_eingabe).
Jetzt wollen wir aber noch mal einen Blick darauf werfen, was diese Funktion nach_unten_ausrichten oder eben auch nach_rechts_ausrichten eigentlich tatsächlich tut.
Betrachten wir den Fall, dass wir uns nach unten ausrichten möchten.
Wir bestimmen hierbei, dass die Richtung des Kopfes eben nach unten sein soll sowie in Zeile 3 geschehen,
und damit wir nicht eine 180-Grad-Wende machen, überprüfen wir vorher in Zeile 2, dass wir nicht momentan schon dabei sind, nach oben zu laufen.
Denn wir möchten, dass nur, wenn man momentan nach rechts oder eben nach links geht, dass man dann die Chance hat, sich auch nach unten auszurichten, denn sonst würden wir ja später mit unserem eigenen Körper wieder kollidieren,
das heißt mit den restlichen Segmenten, und das wollen wir hierdurch eben vermeiden.
So wie wir die Funktion nach_unten_ausrichten definiert haben, folgen dann also die anderen drei Funktionen, damit wir uns in die jeweilige Richtung ausrichten können.
Wir achten dabei hierauf, dass wir nur den Kopf entsprechend ausrichten müssen, denn die anderen Körperteile werden später dem Kopf folgen und brauchen daher nicht ausgerichtet zu werden.
Dafür brauchen wir also die direction, die wir ganz am Anfang definiert hatten.
Jetzt ist diese also richtig gesetzt, und jetzt können wir sie eben verwenden, um den Kopf zu bewegen.
Dafür definieren wir uns auch eine Funktion, die genauso heißt, kopf_bewegen,
und ich will mir mal zuallererst anschauen, wie sieht das aus, wenn wir jetzt mit dem Kopf eben nach unten laufen wollen?
Also wir haben den gerade auf "down" gesetzt.
Dann wollen wir zuallererst die aktuelle Koordinate von dem Kopf abrufen, das können wir mit der Funktion ycor machen.
Die rufen wir einfach ganz normal durch einen Punkt getrennt wieder auf unserer Turtle auf und erhalten damit dann die y-Koordinate von dem Kopf zurück.
Und dann wollen wir eben diese Koordinate nehmen und verändern, je nachdem, wie wir gerade laufen wollen.
Aktuell wollen wir ja nach unten laufen, das heißt, das sehen wir hier auch gerade, dass wir die y-Koordinate um -20 verändern wollen.
Damit erzeugen wir eben diese Runter-Bewegung, das heißt also, wir nehmen den y-Wert, den wir uns gerade geholt haben und inkrementieren den eben um 20.
Das Ganze wollen wir dann natürlich wieder auf die y-Koordinate vom Kopf setzen, und dafür, wie der Name schon sagt, gibt es die Funktion sety, also, um den y-Wert von dem Kopf eben zu setzen.
Den rufen wir auch wieder ganz normal auf, wie ihr das kennt.
Und dann haben wir eigentlich schon alles gemacht, was wir tun wollen, um eben den Kopf bewegen zu können, wenn die direction auf "down" gesetzt ist.
Genauso kann man dann fortfahren für die anderen Richtungen, hier zum Beispiel für rechts.
Dabei müsst ihr dann darauf achten, dass wir nicht mehr die Funktion ycor und sety genommen haben, sondern das Ganze mit x gemacht haben, also xcor und setx
und eben auch nicht -20, sondern +20 gemacht haben, weil wir uns ja nun eben in dem Fall nach rechts auf der x-Achse bewegen wollen.
Also wir müssen ein bisschen aufpassen bei den anderen Bedingungen, muss ich ein Minus muss ein Plus verwenden, und was für Funktionen will ich eigentlich wirklich verwenden.
Genau. Und somit haben wir dann auch den nächsten Baustein wieder uns fertig definiert, nämlich kopf_bewegen.
Schauen wir uns doch einmal an, was wir soweit schon geschafft haben.
Insbesondere möchten wir einfach schon mal unseren Schlangenkopf bewegen, damit wir sehen, dass alles soweit funktioniert.
Und wenn wir noch einmal zurückgehen zu der Funktionsdefinition von interpretiere_eingabe, dann haben wir hierbei ja nur das Ausrichten
nach unten, rechts oder eben in die anderen Richtungen gemacht und noch nicht selber den Kopf bewegt.
Das wäre aber doch schön, wenn wir das hier schon machen würden direkt.
Daher ergänzen wir, und zwar nur für zwischendurch, hier in Zeile 9 den Aufruf von kopf_bewegen.
Das ist also genauso, wenn wir in diesen Baustein von dem Aufruf onclick und interpretiere_eingabe
noch zwischendurch jetzt den neuen Baustein kopf_bewegen hineinschieben, um den hier zu verwenden, und dann eben später wieder hinausnehmen.
Und wenn wir uns das im Video ansehen, dann sehen wir, dass unsere Bewegung schon funktioniert,
wir also durch die einzelnen Aufrufe der Pfeiltasten unseren Schlangenkopf durch das Spielfeld bewegen können, aber bislang eben noch nicht das Essen einsammeln oder zum Beispiel überprüfen, dass wir mit dem Rand kollidieren denn das haben wir noch nicht gemacht.
Aber genau das wollen wir uns dann eben später anschauen.
Genau. Wir haben also zusammenfassend gesehen, dass wir uns verschiedenste Bausteine für unser Spiel definiert haben.
Zum einen eben einen großen Baustein, der die verschiedenen Grafiken zusammenfasst, die wir auf dem Spielfeld sehen.
Und zum anderen haben wir ganz verschiedene Funktionen definiert, wovon wir uns vor allem die Aufrufe onclick, interpretiere_eingabe und eben kopf_bewegen merken wollen.
Und vielleicht auch noch mal kurz nochmal betont eben, kopf_bewegen haben wir gerade temporär in interpretiere_eingabe reingeschoben, wir wollen es aber später wieder rausnehmen und einzelnen verwenden.
Im letzten Video haben wir euch gezeigt, wie ihr bereits dieses Grundspiel erzeugen könnt und wie wir dafür sorgen, dass über die Steuerungsdreiecke in der rechten unteren Ecke unser Schlangenkopf, also das schwarze Quadrat, sich über den Bildschirm bewegen kann.
Bislang ist es aber so, dass wir das rote Essen, also den roten Kreis, noch nicht einsammeln und dadurch auch nicht länger werden.
Das wollen wir in dieser Lektion angehen und schauen, wie unsere Schlange durch das Aufsammeln von Essen wachsen kann.
Deshalb schauen wir uns zuallererst also die Kollision mit dem Essen an, dafür definieren wir uns die Funktion checke_kollision_mit_essen.
Wir wollen also sehen, gibt es überhaupt gerade eine Kollision damit?
Wenn also der Schlangenkopf sich neben dem Essen befindet, sind diese beiden Elemente ja wieder 20 hoch und 20 breit. Das heißt, vom Mittelpunkt aus betrachtet sind die beiden wirklich 10 Punkte jeweils zum Rand entfernt und somit 20 Schritte auseinander.
Und in dem Moment, wenn sie also näher als 20 Schritte voneinander entfernt sind, wieder vom Mittelpunkt aus betrachtet, haben wir also eine Kollision von dem Kopf mit dem Essen.
Und genau das wollen wir jetzt abfragen, ob das eben der Fall ist, dafür nutzen wir die Funktion distance.
Die kennt ihr noch nicht, aber die kann man eben auch auf einer Turtle aufrufen.
Wenn wir das also schreiben, wie ihr das da seht, dann finden wir den Abstand von dem Kopf und dem Essen heraus, und eben wenn der kleiner 20 ist, haben wir eine Kollision.
In diesem Fall wollen wir zwei verschiedene Dinge tun, zuallererst wollen wir unser Essen an eine neue Position bewegen, denn es wurde ja gerade gegessen und muss wieder neu positioniert werden.
Und als zweites wollen wir die Schlange wachsen lassen, denn sie hat ja, wie gesagt, gerade gegessen und soll jetzt eben größer werden.
Um das überhaupt tun zu können, muss natürlich die Schlange auch einen Körper besitzen, und dafür definieren wir uns die Liste segmente.
In der wollen wir später die verschiedenen Turtles reintun, die eben die verschiedenen Teile von unserer Schlange definieren.
Also das sind dann die verschiedenen, gelben Quadrate, aus denen der Schlangenkörper besteht.
Das sind alles einzelne Turtles, und die werden dann eben in dieser Liste verwaltet.
Wir sehen also, dass der Baustein für die verschiedenen Grafiken sich noch um eine letzte, ja, um ein letztes Symbol erweitert, nämlich um die Klammern.
Die haben wir einfach übernommen von der Liste eben, und wir sehen also, der Baustein enthält jetzt diese vier Elemente.
Wie die verschiedenen Teile umgesetzt werden, also Teil eins und Teil zwei, werden wir euch jetzt im Folgenden erklären.
Beginnen wir also zunächst mit dem ersten Teil, nämlich das Essen an eine neue, zufällige Position zu bewegen.
Dazu als Hilfe wieder unser Spielfeld mit dem Raster eingezeichnet.
Wenn wir uns nun den linken Rand einmal näher ansehen, dann wissen wir, dass unser Spielfeld bis geht
und unsere erste Linie dann also bei -190 entsprechend liegt, nämlich genau in diesem Abstand von 10 von dem linken Rand, von -200 entfernt.
Und die zweite Rasterlinie befindet sich dann eben bei -170, das heißt, maximal kann eben unser Essen in diesem Feld liegen,
denn alles, was weiter nach links wäre, wäre ja außerhalb des Spielfelds und nur dieses halbe Feld.
Und wir haben hier also, wenn wir uns jetzt auf der Mittellinie befinden, das heißt, das Raster geht zu +10 und -10, dann haben wir den Mittelpunkt von unserem Essen, der bei minus -180 und 0 liegt.
Und so können wir uns eben vom linken Spielfeldrand allmählich nach rechts bewegen, das heißt, das Feld direkt daneben, in das also Essen platziert werden kann, das hätte dann die Koordinaten -160 und eben 0.
Wir bewegen uns also mit dem Essen genauso in Zwanzigerschritten, wie wir das bei dem Schlangenkopf getan haben.
Und dafür brauchen wir also jetzt eine zufällige Zahl, die wir generieren, damit eben das Essen irgendwo auf dem Spielfeld landet.
Wir denken uns also eine zufällige Zahl, die, multipliziert mit 20, genau in diesen Wertebereich fällt, also minimal bei -180 liegt, dann-160 und so weiter, bis wir schließlich bei +180 angekommen sind.
Diese zufällige Zahl, die soll also jetzt unsere x-Koordinate bilden, damit wir uns irgendwo auf der Horizontalen das Essen platzieren.
Und genau so soll das Ganze für y geschehen, damit wir auch in die Höhe gehen und irgendwo zufällig unser Essen platzieren können.
Wir haben jetzt also schon gesehen, wie wir die Koordinaten von unserem Essen definieren müssen, damit wir eben in unserem Raster laufen.
Aber es gibt noch eine zusätzliche Bedingung, die die neue Positionen vom Essen beinhalten muss,
nämlich sie darf nicht über der Steuerung platziert werden. das wäre ja ein bisschen unpraktisch.
Schauen wir uns dafür also noch mal genau an, wo die Steuerung liegt.
Die Steuerungselemente nach oben und nach rechts hatten ja die Koordinaten 140 und -160 und 160 und -140.
Das heißt also, dass in dem Moment, wenn unsere x-Achse von der neuen Essensposition &gt;= 140 ist und die y-Koordinate &lt;= -140, wir uns ganz sicher eben in diesem Bereich von der Steuerung befinden
und dann eben aber nicht die gerade neu berechnete Position nehmen können, sondern nochmal neu eine zufällige, neue Position berechnen müssen.
Das Ganze haben wir mal versucht, ein bisschen als Ablaufdiagramm darzustellen.
Das heißt also, wir fragen uns immer, liegt die Position im Bereich der Steuerung, also entsprechen die Koordinaten eben den Werten, wie wir es gerade gesehen haben.
Und immer wenn das also der Fall ist, dann müssen wir oder wollen wir das Essen an diese Position bewegen.
Aber wenn das eben auf der Steuerung liegt, dann müssen wir die Werte nochmal neu berechnen.
Wir wollen also nochmal neu zufällige Werte eben erzeugen, weil wir dann eben das Essen sonst auf der Steuerung platzieren würden.
Und immer dann beginnt der Kreis quasi nochmal von Neuem, weil wir dann nochmal prüfen müssen, sind jetzt die neu generierten Werte nicht über Steuerung oder eben doch.
Um sicherzustellen, damit wir mindestens einmal tatsächlich auch eine neue Position berechnen, setzen wir zu Beginn das Essen also absichtlich mit den Werten genau über diesen Bereich der Steuerung.
So ist sichergestellt, dass wir an dieser Stelle, wo wir dann abfragen, ob wir uns über der Steuerung befinden oder nicht, eben definitiv den rechten Zweig wählen, das heißt, ja, und eine neue Position wählen.
Und so wird eben das Essen garantiert auch an eine neue Position verschoben und bleibt nicht immer auf dem Spielfeldrand genau dort liegen, wo es zuvor eben war.
Wenn wir nun uns wieder zurück an das Schema dieser Funktion checke_kollision_mit_essen erinnern, dann hatten wir eben genau die zwei Teile, nämlich zum einen das Essen an eine Position zu bewegen und eben die Schlange wachsen zu lassen.
Und den Teil eins, nämlich das Bewegen an eine neue Position, das haben wir jetzt erfolgreich abgeschlossen.
Wir suchen uns also eine zufällige Zahl in diesem Zwanziger-Schritt-Bereich und bewegen dann eben das Essen genau dorthin.
Nun wollen wir uns den zweiten Teil anschauen.
Dort steht ja, dass wir die Schlange wachsen lassen wollen, auch das haben wir mal ein bisschen in einem Ablauf dargestellt.
Immer wenn wir die Schlange also um ein gelbes Kästchen erweitern wollen, müssen wir ein neues gelbes Kästchen erstellen.
Das ist also immer eine neue, eigene Turtle, wir wollen also wieder diesen Turtleaufruf verwenden.
Wenn wir die Turtle dann haben, dann müssen wir noch verschiedene Eigenschaften von der Turtle anpassen,
also zum Beispiel darf die Farbe natürlich nicht schwarz sein wie vom Kopf, sondern wir wollen ja eine gelbe Farbe haben.
Also man kann sich so ein bisschen an den Eigenschaften orientieren, die für den Kopf gesetzt sind, aber nicht bei allen kann man das komplett so übernehmen.
Und zu allerletzt wollen wir dann dieses neue Körpersegment an unsere Liste segmente eben anfügen.
Wir haben also jetzt auch den zweiten Teil zusammen und sehen also, dass wir die verschiedensten Dinge mit dem neuen Körpersegment machen wollen und es am Ende eben zufügen.
Und somit haben wir die gesamte Funktion eben zusammen.
Wir sehen also, dass die gelben Turtles weiterhin noch in der Mitte bleiben, wenn wir uns jetzt bewegen und hier spielen.
Das liegt daran, dass wir eben noch nicht die Position von denen gerade hinzugefügten gelben Turtles neu setzen.
Insgesamt haben wir also unseren ersten Baustein für die verschiedenen Kollisionen definiert, nämlich den Baustein checke_kollision_mit_essen.
Während wir uns dann also im nächsten Video anschauen werden, wie wir tatsächlich auch die Kollision mit dem Fensterrand überprüfen und eben auch die einzelnen Körperteile bei der Bewegung mitnehmen,
haben wir in dieser Lektion also noch mal gesehen, wie wir insgesamt die verschiedenen Elemente definieren müssen, nämlich ergänzt um die Liste für die einzelnen Körperteile, die wir Segmente nennen.
Und dann eben die bestehenden Bausteine mit den zwei Funktionsaufrufen ergänzt eben um den dritten Baustein, um die Kollision mit dem Essen zu überprüfen
und eben schon neue Körperelemente zu erstellen, auch wenn diese momentan noch in der Mitte liegen bleiben.
Video herunterladen
Zusätzliches Material herunterladen
Wir wollen uns heute die beiden weiteren Kollisionen anschauen und schauen, wie wir uns nicht nur über einen Klick bewegen können.
Dafür gucken wir uns zuerst die Kollision mit den Fensterrand an, denn wenn wir mit dem kollidieren, haben wir das Spiel auch verloren.
Dafür definieren wir uns zuallererst die Funktion checke_kollision_mit_fensterrand.
Wenn wir uns also nun über den Fensterrand zum Beispiel auf dem linken Spielfeldrand bewegen, also über die -190 laufen, dann haben wir zum einen verloren.
Das heißt also, wir überprüfen genau den Fall, wenn die x-Koordinate vom Kopf kleiner als -190 ist.
Und auf der anderen Seite vom Spielfeld haben wir den Fensterrand überschritten, wenn wir eben über diese 190 laufen.
Und genau das fragen wir auch hier eben ab.
Genauso können wir auch auf der y-Achse über den Spielfeldrand hinauslaufen, und das fragen wir hier also dem zweiten Teil ab.
Und das heißt also, wenn eine von diesen Bedingungen zutrifft, darum sind die auch mit or verknüpft, in dem Fall haben wir eine Kollision mit dem Fensterrand.
Dann wollen wir verschiedene Dinge tun.
Zuallererst wollen wir den Kopf in der Mitte platzieren.
Wir wollen überhaupt jetzt hier in den nächsten Zeilen unser Spiel quasi neu starten, wenn man so will.
Also wir wollen alle Dinge so setzen, dass wir wieder ein Spiel neu beginnen können.
Dafür platzieren wir, wie gesagt, den Kopf in der Mitte, das ist immer unsere Ausgangsposition vom Kopf.
Dann setzen wir die Richtung vom Kopf auf "stop", das heißt, wenn der gerade auf "ab" oder so gesetzt ist,
wollen wir eben genau das unterbinden, damit der Kopf nicht weiter in irgendeine Richtung läuft, wenn das Spiel neu beginnt, begonnen werden soll.
Und als Nächstes rufen wir die Funktion segmente_entfernen auf.
Diese Funktion ist von uns, die ist dafür da, dass die Segmente eben entfernt werden und dann die segmente-Liste eben damit geleert wird.
Als allerletztes könnt ihr noch eine nette Ausgabe hinzufügen, dass derjenige leider gerade das Spiel verloren hat, weil er eben über den Fensterrand gelaufen ist.
Das sind also Dinge, die wir grundsätzlich beim Neustart eines Spiels machen möchten und die eben nicht nur auf die Kollision mit dem Fensterrand zutreffen,
sondern wir haben ja auch noch gleich die Kollision mit den eigenen Körperteilen, also den Segmenten.
Daher ist es vorteilhaft, wenn wir hier an dieser Stelle die vier Zeilen, das heißt also, Dinge, die wir eben tun wollen, in eine eigene Funktion packen.
Das heißt, wir sagen hier einfach nur, wir wollen das Spiel neu starten und haben dann eine eigene Funktion spiel_neu_starten,
die eben genau diese vier Elemente wieder beinhaltet, um das Spiel neu zu starten und an diesen bekannten Ausgangspunkt zurückzukehren.
Und wenn wir das implementieren und ausführen, zum Beispiel, indem wir absichtlich einfach mal auch nach rechts laufen,
dann sehen wir auch, dass in dem Moment, in dem wir rechts am Fensterrand sind, wir unseren Schlangenkopf zurück in die Mitte setzen und die entsprechende Ausgabe erhalten, dass wir das Spiel leider verloren haben.
Und so ist unser nächster Baustein komplett, nämlich die Kollision mit dem Fensterrand zu überprüfen und gegebenenfalls eben das Spiel neu zu starten, wenn notwendig.
Genau das wollen wir auch tun, wenn wir, wie du ja gerade schon meintest, mit den Segmenten kollidieren.
Das bedeutet also, wir haben das Spiel verloren, wenn wir eben mit dem Schlangenkopf in den Schlangenkörper laufen.
Auch dafür müssen wir verschiedene Bedingungen noch hinzufügen, bevor wir das Spiel neu starten.
Zuallererst wollen wir also für alle Segmente schauen, ob sie mit dem Kopf kollidieren.
Dafür schreiben wir eben die for-Schleife wie in Zeile 2, und jetzt habt ihr ja gerade schon hier gesehen,
wir sind gerade nach oben gelaufen, dann nach rechts, dann nach unten, und wenn wir jetzt nach links laufen würden, würden wir ja mit dem Körper kollidieren, also mit dem Kopf und dem Körper.
Und da sehen wir also, dass wir jetzt wieder 20 Schritte entfernt sind, also der Mittelpunkt vom Kopf und der von dem Segment, mit dem wir kollidieren, diese beiden Mittelpunkte sind unter 20 Schritte voneinander entfernt, wenn sie kollidieren,
und darum fügen wir hier eine sehr ähnliche Bedingung hinzu wie bei der Kollision mit dem Essen.
Also genau in diesem Fall haben wir jetzt also die Kollision mit denen Segmenten und wollen dann eben das Spiel neu starten.
Wir rufen also unsere Funktion spiel_neu_starten auf, die wir gerade schon kennengelernt haben, die wir immer aufrufen wollen, wenn wir irgendwie eben das Spiel neu starten wollen.
Wir sehen also, wir laufen hier dann immer über unseren Bildschirm und haben dann immer eine Verlängerung von den Segmenten eben mit dieser Funktion.
Zuallerletzt haben wir uns also unseren nächsten Baustein definiert, nämlich den für die Kollision mit den Segmenten.
Nun wollen wir aber natürlich tatsächlich uns auch mal als komplette Schlange fortbewegen und dazu eben die Einzelsegmente mitnehmen.
Wir ergänzen also eine neue Funktion mit dem Namen koerper_bewegen, die eben genau das macht.
Und unsere Grundidee ist dabei, dass wir zuallererst die einzelnen Teile des Körpers bewegen und anschließend den Kopf nach vorne setzen.
Das heißt, dieser läuft eigentlich sozusagen als letztes.
Das wird man aber im fertigen Spiel dann nicht sehen.
Das hilft uns aber, die Körperteile direkt dorthin zu platzieren, wo sie hinsollen.
Und damit wir das gleichmäßig machen, starten wäre also damit, dass wir von hinten das letzte Körperteil nach vorne bewegen, um einen Schritt.
Und wir können dazu die Funktion range wiederverwenden.
Diese habt ihr bereits früher kennengelernt, als wir die Schleifen eingeführt haben, und damals haben wir range mit einem Argument, nämlich zum Beispiel range (5) verwendet, um also 0, 1, 2, 3, und 4 auszugeben.
Hier wollen wir jetzt aber anders zählen.
Wir nutzen also range mit insgesamt 3 Argumenten und wollen die kurz vorstellen.
Range kann also 3 Argumente haben, die wir einfach Start, Ende und Schritt nennen.
Start ist dabei relativ einfach, nämlich von wo wir zählen wollen und wird auch direkt mit ausgegeben.
Ende wird nicht mit ausgegeben und ist also die Maximalzahl, die wir nicht mit ausgeben, und alle davor, von Start beginnend, geben wir also aus.
Und Schritt wiederum kann dazu verwendet werden, um nicht nur in Einserschritten weiter zu zählen, also 0, 1, 2 und so weiter,
sondern eben zum Beispiel nur jede zweite Zahl auszugeben, indem wir 0 und 2 und so weiter zählen,
06:20oder eben auch, indem wir rückwärts zählen, und genau das wollen wir eben gleich tun.
Start ist dabei optional, wie wir gesehen haben, und wenn es nicht angegeben wird, dann geht Python automatisch von 0 aus.
Und Schritt ist genauso optional, und wenn wir Schritt nicht angeben, geht Python eben von 1 aus.
Das heißt, nur Ende wird benötigt, und so kommen wir eben tatsächlich auch auf die Kurz-Schreibweise, die wir euch schon erzählt hatten von eben range (5).
Wir können range aber auch nur mit zwei Argumenten ausführen, zum Beispiel indem wir range (2, 5) schreiben, um dann eben die drei Zahlen 2, 3 und 4 zu erhalten und diese auszugeben.
Das ist gleichbedeutend, wenn wir range (2, 5, 1) schreiben, wenn wir also explizit noch die Schrittgröße mit angeben.
Und genau das können wir uns jetzt zunutze machen, um eben rückwärts zu zählen.
Dazu müssen wir wirklich auch alle drei Argumente mit angeben und können dann eben range (5, 2, -1) zum Beispiel schreiben und geben dann die Zahlen rückwärts aus.
Achtet aber bitte hier darauf, dass wir beim Rückwärtslaufen tatsächlich dann auch die 5 natürlich mit ausgeben, weil der Start-Index ist ja immer mit dabei, und eben die 2 nicht mit ausgeben, denn das Ende ist ja nie mit eingeschlossen.
Das gilt hier also auch.
Und zurück zu unserem eigentlichen Bewegen des Klangkörpers.
Da hatten wir also genau den Teil, wir gehen rückwärts durch die einzelnen Elemente des Schlangen Körpers durch und nutzen den Index.
Wir möchten nun also das letzte Element des Schlangenkörpers um ein Element nach vorne versetzen, also nutzen wir index und index -1.
Lass uns das doch einmal veranschaulichen.
Dazu können wir also diese Grafik verwenden, mit einem Körper-Kopf und eben den drei Körper-Elementen.
Diese wollen wir jetzt also in Fallrichtung, das heißt einmal nach rechts bewegen.
Wir verwenden nun zur Hilfe wieder die Indexzahlen, und als erstes bewegen wir also das letzte Körperteil, das mit index 2 auf das Körperteil, das momentan index 1 hat.
Unsere Schlange wird also quasi kürzer, und zwei Körperteile liegen aufeinander.
Das ist aber vollkommen in Ordnung, und genauso verfahren wir jetzt, um das Körper-Element mit dem index 1 auf das mit dem Index 0 zu legen.
Damit ist unsere for-Schleife abgearbeitet, denn 0 überprüfen wir hier nicht mehr, denn der Endindex ist ja immer exklusive.
Wir möchten nun aber natürlich noch weiter uns bewegen, und das Ziel ist es, weil wir den Kopf als letztes bewegen, dass wir das Element direkt hinter dem Kopf, also in diesem Schritt gerade, auf den Kopf drauflegen.
Dazu müssen wir nicht natürlich zuerst überprüfen, dass wir tatsächlich auch Körperelemente haben, denn sonst funktioniert das Ganze nicht.
Und wenn das der Fall ist, dann wollen wir also das Körperelement mit dem Index 0 auf die aktuelle Position des Kopfes legen.
Wenn wir das machen, ist der Kopf also kurz verdeckt.
Das ist aber nicht schlimm, denn wir werden im Folgenden den Kopf noch um einen Schritt bewegen, und dann ist unsere Schlange wieder ganz normal.
Vielleicht habt ihr an dieser Stelle auch schon die Einrückung bemerkt.
Das ist genauso, wie wir in Python hinterher auch die einzelnen Codezeilen schreiben werden, um eben die Bewegung des Körpers zu vollziehen.
Und da wir hier nur die einzelnen Körperteile bewegen und nicht den Kopf, ist damit unsere Funktion vollendet.
Wir haben den nächsten Baustein kennengelernt, der koerper_bewegen heißt
Nun wollen wir all diese Bausteinen, die wir kennengelernt haben, mal ineinander stecken, denn dafür sind Bausteine ja da.
Als allererstes stecken wir dafür den Baustein für die verschiedenen Grafiken und den Baustein für die verschiedenen Funktionsdefinitionen zusammen.
Den seht ihr hier gerade als allererstes, aber das soll einfach mal diese ganzen Definitionen zusammenfassen.
Danach stecken wir noch den Aufruf onclick (interpretiere_eingabe) mit dazu, denn wir wollen ja auch immer zuallererst überhaupt überprüfen, wohin läuft der jetzt die Schlange als nächstes.
Danach fügen wir die beiden Funktionen für die Kollision einmal mit dem Essen und einmal mit dem Fensterrand hinzu.
Und danach bewegen wir jetzt also unsere Schlange.
Wie wir gerade schon gesehen haben, bewegen wird dazu immer erst den Körper und dann den Kopf.
Wenn wir uns dann mit der Schlange fortbewegt haben, wollen wir dann auch noch überprüfen, sind wir gerade mit uns selber kollidiert, haben wir also deshalb vielleicht gerade das Spiel verloren?
Dafür eben dann der letzte Aufruf.
So sieht also jetzt unser gesamter Spielablauf aus, so steckt man die verschiedenen Steine ineinander.
Aber wenn man sich das Ganze eben mal genauer anschaut, sehen wir schon, dass wir die letzten fünf Bausteine eigentlich immer wieder aufrufen wollen und nicht nur einmal.
Wenn wir jetzt die Funktionen ja so untereinander geschrieben hätten, würden wir ja jede nur einmal aufrufen.
Und deshalb betrachten wir uns mal diese fünf Steine genauer und lagern sie in eine Extra-Funktion aus.
Dafür haben wir wiederum die Funktion wiederhole_spiellogik implementiert, in der einfach wirklich diese fünf Funktionsaufrufe drinstehen,
und wir haben quasi dafür gesorgt, dass also jetzt dieser Spielstein immer wieder jede Sekunde quasi aufgerufen wird.
Ihr seht also hier den Baustein, der hinzukommt, mit wiederhole_spiellogik.
Und wenn wir diesen jetzt noch mit ergänzen wollen, dann separieren wir wieder unsere Bausteine, wir heben also die oberen drei davon ab, sodass eine Lücke entsteht,
und fügen jetzt die unteren fünf Bausteine so ein, dass sie eben innerhalb dieses neuen Bausteins wiederhole_spiellogik sind.
So sorgen wir dafür, dass der obere Teil, das heißt, die oberen drei Bausteine, nur ein einziges Mal ausgeführt werden, denn wir wollen ja zum Beispiel nur einen Schlangenkopf haben und die unteren fünf Elemente, die jetzt von diesem blauen Baustein umklammert werden, immer wieder ausgeführt werden.
Und wenn wir das haben, dann können wir endlich auch zum ersten Mal unser Spiel genießen.
Wir können also unseren Schlangenkopf bewegen, wir können das Essen einsammeln, wir können dadurch wachsen.
Und solange wir nicht mit dem Fensterrand kollidieren oder eben mit den einzelnen Segmenten unseres eigenen Körpers, können wir spielen und tatsächlich einfach Spaß haben.
Ihr könnt hierfür auch die Tastatur nutzen.
Das heißt, sobald die Steuerung mit der Maus funktioniert, durch das Klicken auf die einzelnen Steuerungs-Dreiecke, könnt ihr die Pfeiltasten benutzen, um eure Schlange zu bewegen.
Das macht es viel leichter, länger zu werden und mehr Spaß zu haben.
Und wenn ihr Lust hat, dann könnt ihr noch deutlich mehr hinzufügen, also zum Beispiel einen Punktezähler, ihr könnt verschiedene Runden machen, die Schwierigkeit erhöhen oder eben was euch so einfällt.
Und teilt doch vielleicht eure kreativsten Ideen mit uns im Forum.
Wir sind jetzt nun leider also am Ende von unserem Kurs angekommen.
Die nächste Lektion ist nur ein optionaler Exkurs.
Das heißt, wir wollen uns an dieser Stelle ganz herzlich bei euch für die aktive Teilnahme am Kurs bedanken und hoffen, ihr hattet viel Spaß dabei und habt hoffentlich auch viel gelernt.

