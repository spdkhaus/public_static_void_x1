Zahlenraten
•	Ziel des Spiels: Mit so wenigen Anfragen wie möglich die Zahl des Computers erraten
Programmablauf

1.	Am Anfang wird mit der Funktion randint(1,100) eine zufällige Zahl zwischen 1 und 100 festgelegt.
2.	Der Nutzer kann eine Zahl eingeben. Dafür verwenden wir die Funktionen input() und int().
3.	Mit Hilfe einer Verzweigung wird überprüft, ob die eingegebene Zahl zu groß, zu klein oder die richtige Zahl ist.
4.	
o	Hat der Nutzer bereits die richtige Zahl eingegeben, ist das Spiel zu Ende.
o	Hat der Nutzer noch nicht die richtige Zahl eingegeben, wiederholen sich die Schritte von 2 bis 4 solange, bis die richtige Zahl erraten wurde.
o	Dieses Wiederholen wird mit Hilfe einer While-Schleife und einer Variable gefunden ermöglicht.
o	Es wird ein ValueError wird mit Hilfe der Schlüsselwörter try: und except: ValueError abgefangen.
Das Zahlenraten in Python funktioniert wie folgt:
1.	Zufallszahl generieren: Eine zufällige Zahl innerhalb eines bestimmten Bereichs wird erzeugt.
2.	Eingabe des Spielers: Der Spieler gibt eine Zahl ein.
3.	Vergleich: Die Eingabe des Spielers wird mit der zufälligen Zahl verglichen.
4.	Rückmeldung: Dem Spieler wird mitgeteilt, ob die Eingabe zu hoch, zu niedrig oder korrekt ist.
5.	Wiederholung: Die Schritte 2 bis 4 werden wiederholt, bis der Spieler die richtige Zahl errät.


