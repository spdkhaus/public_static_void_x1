Aufgabe 2.1: 

Die Teilnehmer werden auf zwei Wohnhäuser aufgeteilt. Importiere die Variable alter, um einen Integer für diese Variable zu erhalten. Schreibe dann eine Verzweigung, die für die Variable testet, ob diese größer als 10 ist. Wenn ja, soll 

Haus Regenbogen 

ausgegeben werden. Wenn nicht, soll die Ausgabe 

Haus Sonnenschein 

  lauten. 

 

Lösung: 

from daten import alter 

if alter > 10: 

    print("Haus Regenbogen") 

else: 

    print("Haus Sonnenschein") 

 

Aufgabe 2.1.2: 

Innerhalb von einem Haus werden die Teilnehmer nach ihren Namen aufgeteilt. Alle, die einen Anfangsbuchstaben haben, der im Alphabet vor "M" kommt, kommen in die erste Etage. Alle anderen kommen ins Erdgeschoss. Importiere dafür die Variable name aus der Bibliothek daten und gib entweder 

{name} kommt in die erste Etage.  

oder 

{name} kommt ins Erdgeschoss.  

aus. 

Lösung: 

from daten import name 

if name < 'M': 

    print(name + " kommt in die erste Etage.") 

else: 

    print(name + " kommt ins Erdgeschoss.") 

Aufgabe 2.1.3: 

Die Campteilnehmer teilen sich nun auf die Zimmer auf. Importiere dafür aus der Bibliothek daten die Variable max_anzahl, die die Anzahl an Betten in dem Zimmer speichert, und die Variable belegt, die die Anzahl an belegten Betten speichert. Prüfe, ob noch mindestens ein Bett in dem Zimmer frei ist. Wenn dies der Fall ist, gebe 

Ich gehe in dieses Zimmer. 

aus. Falls das Zimmer schon voll ist, gebe 

Schade, das Zimmer ist schon voll. 

aus. 

Lösung: 

from daten import max_anzahl, belegt 

if belegt < max_anzahl: 

    print("Ich gehe in dieses Zimmer.") 

else: 

    print("Schade, das Zimmer ist schon voll.") 

     

Aufgabe 2.2.1: 

Simon, Leonie und zwei weitere Freunde suchen einen Tisch, an dem sie alle vier sitzen können und der zusätzlich am Fenster oder an der Tür steht. Nutze dafür die Variablen sitze (ein Integer) und ort (ein String) aus der Bibliothek daten. 

Wenn der Tisch genau vier Plätze hat und am Fenster oder der Tür steht, gebe 

Juhu, den Tisch nehmen wir. 

aus. 

Wenn der Tisch nicht genau 4 Plätze hat, gebe 

Wir brauchen einen Tisch mit genau 4 Sitzen. 

aus. 

Wenn der Tisch vier Plätze hat, aber weder am Fenster noch an der Tür steht, gebe 

Der Tisch hat zwar die richtige Größe, ist aber weder am Fenster noch an der Tür. 

aus. 

Lösung: 

from daten import sitze, ort 

if sitze == 4 and ort == "Fenster" or ort == "Tuer": 

    print("Juhu, den Tisch nehmen wir.") 

elif sitze != 4: 

    print("Wir brauchen einen Tisch mit genau 4 Sitzen") 

elif sitze == 4 and ort != "Fenster" and ort != "Tuer": 

    print("Der Tisch hat zwar die richtige Größe, ist aber weder am Fenster noch an der Tür") 

Aufgabe 2.2.2: 

Die Personen im Camp durften festlegen, was sie zum Mittag essen wollen. Bei der Essensausgabe wird nun anhand der Wünsche für die Hauptspeise (hauptspeise) und den Nachtisch (nachtisch) zugeteilt, wem welches Essen gehört. Kannst du dabei helfen? Es soll der Name der Person ausgegeben werden, der das Essen gehört. Dabei gilt folgendes: 

Simon möchte Suppe und zum Nachtisch Schokoladenpudding. 

Leonie ist die Hauptspeise egal, Hauptsache es sind keine Kartoffeln und zum Nachtisch Obstsalat. 

Paul möchte Kartoffeln oder Nudeln und zum Nachtisch Erdbeerjoghurt. 

Mia isst jegliche Kombination aus Hauptspeise und Nachtisch, die keiner der anderen will. 

Lösung: 

from daten import hauptspeise, nachtisch 

 

if hauptspeise == 'Suppe' and nachtisch == 'Schokoladenpudding': 

    print('Simon') 

elif hauptspeise != 'Kartoffeln' and nachtisch == 'Obstsalat': 

    print('Leonie') 

elif (hauptspeise == 'Kartoffeln' or hauptspeise == 'Nudeln') and nachtisch == 'Erdbeerjoghurt': 

    print('Paul') 

else: 

    print('Mia') 

 

Aufgabe 2.2.3: 

Simon sagt seinen Sitznachbarinnen und Sitznachbarn am Tisch, ob sie ihr Essen in Form von einem Quadrat oder von einem Dreieck legen sollen. Du bekommst dafür in der Variable form ein String “Quadrat” oder “Dreieck”. Je nachdem, was du bekommst, sollst du entweder ein Quadrat mit Seitenlänge 100 oder ein Dreieck malen. 

Lösung: 

from daten import form 

import turtle 

 

if form == 'Quadrat': 

    turtle.forward(100) 

    turtle.left(90) 

    turtle.forward(100) 

    turtle.left(90) 

    turtle.forward(100) 

    turtle.left(90) 

    turtle.forward(100) 

elif form == 'Dreieck': 

    turtle.forward(100) 

    turtle.left(120) 

    turtle.forward(100) 

    turtle.left(120) 

    turtle.forward(100) 

     

Aufgabe 2.3.1: 

Beim Essen kam Simon die Frage, was die Summe der Zahlen von 1 bis 10 ist. Kannst du ihm bei der Berechnung helfen, indem du einfach eine For-Schleife verwendest und das Ergebnis der Summe schließlich ausgibst? 

Lösung: 

n = 0 

for i in range (11): 

    n += i 

print(n) 

 

Aufgabe 2.3.2: 

Bis zur Nachtruhe bleiben noch 100 Minuten zum Spielen übrig. Eine Spielrunde dauert 17 Minuten. Die Kinder wollen so viele Runden wie möglich spielen und vor dem Beginn jeder Runde überprüfen, ob sie noch genug Zeit haben, um eine komplette Runde zu spielen. Jedes Mal, wenn sie noch ausreichend Zeit haben, soll 

Wir spielen noch eine Runde. 

ausgegeben werden und die verbleibende Zeit entsprechend reduziert werden. Wenn die Zeit nicht mehr reicht, soll 

Nun ist Zeit zum Schlafen. 

ausgegeben werden. 

Lösung: 

n = 100 

while n > 17: 

    n -= 17 

    print("Wir spielen noch eine Runde.") 

print("Nun ist Zeit zum Schlafen.") 

Aufgabe 2.3.3: 

Leonie schaut sich in der Nacht den Himmel mit den Sternen an. Sie möchte einen Stern zeichnen, der mindestens 10 Ecken hat und bei dem der Startpunkt gleich der Endpunkt ist. Kannst du ihr dabei helfen? Leonie erinnert sich noch daran, dass der Stern bei 10 Ecken mit einem Winkel von genau 108 Grad besonders einfach zu zeichnen ist. 

Lösung: 

from turtle import *  

 

for n in range(10): 

    forward(100) 

    left(108) 

    fillcolor("yellow")  

 