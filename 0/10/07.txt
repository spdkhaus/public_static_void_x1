Verzweigungen 

Dadurch werden gewisse Programmzeilen nur unter bestimmten Bedingungen ausgeführt 

Bedingungen können Vergleiche und Boolsche Aussagenlogik enthalten 

if Bedingung: 

   Aktion1 

else: 

   Aktion2 

Aktion1 wird ausgeführt, wenn die Bedingung wahr ist 

Aktion2 wird ausgeführt, wenn die Bedingung falsch ist 

if Bedingung1: 

    Aktion1.1 

    Aktion1.2 

    ... 

elif Bedingung2:  

    Aktion2.1  

    ... 

elif Bedingung3: 

    Aktion3.1  

     ... 

else:  

    ... 

Bedingungen werden nacheinander geprüft 

Erster Zweig, bei dem die Bedingung wahr ist, wird ausgeführt 

Nachfolgende Bedingungen werden nicht mehr geprüft 

Vergleiche 

Sind entweder True (wahr) oder False (falsch): 

a < b (a ist kleiner als b) 

a <= b (a ist kleiner oder gleich b) 

a > b (a ist größer als b) 

a >= b (a ist größer oder gleich b) 

a == b (a ist gleich b) 

a != b (a ist ungleich b) 

Boolsche Aussagenlogik 

and ist wahr, wenn beide damit verknüpfte Aussagen wahr sind 

or ist wahr, wenn mindestens eine der damit verknüpften Aussagen wahr ist 

Daten importieren 

Mit from <datei> import <name> können nicht nur Funktionen, sondern auch Variablen aus einer Datei eingebunden werden 

Wenn damit Variablen eingebunden werden, wurden ihnen in datei Werte zugewiesen, die man abfragen kann 

 

 

Verzweigungen in Python werden durch die Verwendung von if-else-Anweisungen realisiert. Mit if-else-Anweisungen können Bedingungen überprüft und je nach Ergebnis unterschiedliche Codeblöcke ausgeführt werden. Zum Beispiel: 
 
python 
x = 10 
 
if x > 5: 
print("x ist größer als 5") 
else: 
print("x ist nicht größer als 5") 
 
 
In diesem Beispiel wird überprüft, ob der Wert von x größer als 5 ist. Wenn die Bedingung erfüllt ist, wird der erste Codeblock ausgeführt, andernfalls der zweite.  

 