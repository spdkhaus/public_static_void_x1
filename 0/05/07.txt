String 

Zeichenkette, also eine Folge von Buchstaben und/oder anderen Zeichen 

In Python mit einfachen (') oder doppelten (") Anführungszeichen geschrieben 

Operationen auf Strings 

Verkettung von Strings mit Plusoperator (+) 

Ergibt neuen String 

Zwischen den Strings wird nicht automatisch ein Leerzeichen eingefügt 

Wiederholung von Strings mit Maloperator (*) 

Ergibt neuen String 

Zahl, mit der der String multipliziert wird, gibt an, wie häufig der String wiederholt wird 

len() gibt die Anzahl an Zeichen eines Strings (inklusive der Leer- und Sonderzeichen) zurück 

Platzhalter 

Lücke in einem String, die durch eine Variable ersetzt wird 

Nur im Zusammenhang mit einem F-String möglich, der durch ein f oder F vor dem String gekennzeichnet wird 

Durch geschweifte Klammern im F-String dargestellt, in der Variablenname steht 

Beispiel, das "Stella singt gerne." ausgibt: 

person = "Stella" 

satz = f"{person} singt gerne." 

print(satz) 