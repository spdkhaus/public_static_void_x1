Grundwissen Physik – 10. Jahrgangsstufe
1
Grundwissen Physik – 10. Jahrgangsstufe
I. Astronomische Weltbilder
1. Geozentrisches Weltbild: Planeten und Fixsterne bewegen sich auf Kugelschalen um 
die Erde.
2. Heliozentrisches Weltbild: Die Himmelskörper (außer dem Mond) bewegen sich um die 
Sonne; die Fixsterne sind sehr weit entfernt und ruhen (Kopernikus).
3. Keplersche Gesetze:
1. Die Planeten bewegen sich auf Ellipsenbahnen, in deren einem Brennpunkt die Sonne 
steht.
2. Die gedachte Verbindungslinie Sonne -Planet überstreicht in gleichen Zeitspannen 
gleich große Flächen.
3. Die Quadrate der Umlaufszeiten T zweier Planeten verhalten sich wie die dritten 
Potenzen ihrer großen Halbachsen a.
𝑇1
2
𝑇2
2 =
𝑎1
3
𝑎2
3
4. Modernes Weltbild: Die Sterne sind in Galaxien angeordnet; kein Punkt im Universum 
ist in besondere Weise ausgezeichnet; das Universum ist vor ca. 14 Milliarden Jahren in 
einem Urknall entstanden.
Aufgaben zu I.:
a) Die Umlaufzeit des Modes um die Erde beträgt 27,3 Tage. Die große Halbachse des 
Mondes beträgt 3,84105
km.
Berechne die Umlaufdauer der Internationalen Raumstation ISS, wenn sie auf einer 
Kreisbahn in 400 km Höhe über der Erdoberfläche die Erde umkreist. (rE = 6370 km)
b) Die Umlaufzeit des Planeten Mars um die Sonne beträgt 1,88 Jahre.
Berechne die große Halbachse der Marsumlaufbahn. Gib sie in AE an.
II. Newtonsche Mechanik
5. Die newtonschen Gesetze: 1. Trägheitssatz
2. Kraftgesetz: F = ma
3. Wechselwirkungsgesetz: 𝐹1
⃗⃗⃗ = −𝐹2
⃗⃗⃗ 
6. Harmonische Schwingungen: sinusförmig; werden durch lineare rücktreibende Kräfte
bewirkt: F = ‒D·x ; Frequenz f: 𝑓 =
1
𝑇
 (T: Schwingungsdauer; Einheit von f: 1 Hz = 
1/s).
7. Impuls: 𝑝 = 𝑚 ∙ 𝑣 ; Erhaltungssatz: In einem abgeschlossenen System bleibt der 
Gesamtimpuls erhalten.
8. Kreisbewegung: Winkelgeschwindigkeit: 𝜔 =
∆𝜑
∆𝑡
=
2𝜋
𝑇
; 
Bahngeschwindigkeit: 𝑣 = 𝜔 ∙ 𝑟
Zentripetalkraft: 𝐹𝑍 =
𝑚∙𝑣
2
𝑟
= 𝑚 ∙ 𝜔
2
∙ 𝑟 ; 
Grundwissen Physik – 10. Jahrgangsstufe
2
9. Gravitationsgesetz: 𝐹𝐺 = 𝐺
∗
∙
𝑚∙𝑀
𝑟
2
 (G* =6,671011 m³/(kgs²)) 
10. Relativitätstheorie: Die Lichtgeschwindigkeit c ist überall im Universum konstant und 
unabhängig von der Bewegung eines Beobachters. 
Die Masse eines Körpers vergrößert sich mit seiner Geschwindigkeit.
Gesamtenergie E eines Körpers der Masse m: E = m·c
2
Aufgaben zu II.:
a) Die maximale Startmasse eines Airbus A380 beträgt 560 t. Die maximale Schubkraft jedes 
der vier Triebwerke beträgt 312 kN.
Berechne die maximale Beschleunigung des Flugzeugs auf der Startbahn.
Wie lange muss in diesem Fall die Startbahn mindestens sein, wenn das Flugzeug eine 
Geschwindigkeit von 260 km/h für das Abheben erreichen muss.
b) Max (m1 = 40 kg) und Moritz (m2 = 80 kg) stehen sich auf je einem Skateboard gegenüber.
Plötzlich schubst Max seinen Freund mit der Kraft F.
Erkläre, was passieren wird.
c) Auf einem See schwimmt ein ruhendes Boot der Masse m = 160 kg. Im Boot steht ein Junge 
(m = 60 kg). Der Junge springt mit der Geschwindigkeit v = 3,5 m/s ins Wasser.
Erkläre, was mit dem Boot passiert und berechne die Geschwindigkeit des Bootes.
d) Im Film „Asterix erobert Rom“ wirft Obelix einen Speer mit solcher Geschwindigkeit, dass 
er die Erde umrundet.
Berechne die dafür notwendige Geschwindigkeit des Speers, wenn die Luftreibung 
vernachlässigt wird. (rE = 6370 km)
e) Berechne die Höhe über der Erdoberfläche, bei der die Anziehungskraft halb so groß wie 
auf der Erdoberfläche ist. (MErde = 5,9741024 kg)
III. Wellenlehre und Quantenphysik
11. Wellen: Räumliche Ausbreitung einer Schwingung;
Longitudinalwellen: Ausbreitungs- und Schwingungsrichtung sind parallel.
Transversalwellen: Ausbreitungsrichtung ist senkrecht zur Schwingungsrichtung.
Ausbreitungsgeschwindigkeit v: v = λ·f ; (λ: Wellenlänge; f: Schwingungsfrequenz)
Beugung: Eindringen einer Welle in den Schattenraum.
Interferenz: Überlagerung periodischer Wellen gleicher Frequenz (Verstärkung und
Auslöschung).
12. Licht: Licht ist eine Welle, denn Beugung und Interferenz treten auf.
Licht hat Teilchencharakter: es kann Elektronen aus Körpern herauslösen (Fotoeffekt).
Photonen: Licht-„Teilchen“, die sich mit Lichtgeschwindigkeit ausbreiten und eine 
von der Frequenz abhängige Energie besitzen.
13. Quantenobjekte (z.B. Elektronen, Photonen): besitzen Teilchen- und
Welleneigenschaften; bewegen sich nicht auf Bahnen; nur Wahrscheinlichkeitsaussagen 
für Aufenthalt und Verhalten sind möglich.
Aufgaben zu III.:
a) Nenne je ein Beispiel für eine Longitudinal- und eine Transversalwelle.
Grundwissen Physik – 10. Jahrgangsstufe
3
b) Berechne die Wellenlänge der Schallwelle, die eine Stimmgabel der Frequenz 440 Hz 
aussendet. Die Schallgeschwindigkeit in Luft beträgt bei Normalbedingungen 343 m/s.
c) Beschreibe jeweils einen Versuch, der sich gut mit dem Wellenmodell des Lichts erklären 
lässt sowie einen Versuch, der sich gut mit dem Teilchenmodell des Lichts erklären lässt.
Lösungen:
I. a) Man wendet das 3. Keplersche Gesetz mit der Erde als umkreistes Objekt auf die ISS 
Und den Mond an.
Es gilt:
3
Mond
3
ISS
2
Mond
2
ISS
a
a
T
T


5521s 92min
(3,84 10 m)³
(6770000m)³ (27,3 24 3600s)²
a
a
T T § 8
Mond
3
2 ISS
ISS Mond  

     
b) Man wendet das 3. Keplersche Gesetz mit der Sonne als Zentralgestirn auf die 
Planeten Mars und Erde an.
Es gilt:
3
Erde
3
Mars
2
Erde
2
Mars
a
a
T
T


(1AE)³ 1,5AE
(1a)²
(1,88a)²
a
T
T
a 3 3
2
2 Erde
Erde
2
Mars
Mars     
II. a) Laut dem Grundgesetz der Mechanik gilt: F = ma

s²
m 2,23
560000kg
4 312000N
m
F
a 

 
 v² = 2as  s = v²/(2a)  1,17 km
(Beachte: v = 260 km/h  72,2 m/s)
b) Moritz wird mit a2 = F/m2 beschleunigt.
Aufgrund des Wechselwirkungsgesetzes erfährt Max ebenfalls eine gleich große,
entgegengesetzt wirkende Kraft F und damit ebenfalls eine Beschleunigung.
Diese berechnet sich zu a1 = F/m1 und wirkt in die entgegengesetzte Richtung.
c) Aufgrund des Impulserhaltungssatzes muss der Gesamtimpuls am Ende des Vorgangs
gleich dem Gesamtimpuls zu Beginn sein.
Da der Junge zu Beginn im ruhenden Boot steht, gilt pAnfang = pEnde = 0.
pEnde berechnet sich zu:
pEnde = pJunge + pBoot = 0
 pBoot =  pJunge
mBoot  vBoot =  mJunge vJunge
 vBoot =  (mJunge  vJunge)/mBoot   1,3 m/s
Das Minus zeigt an, dass sich das Boot in die entgegengesetzte Richtung bewegt.
Grundwissen Physik – 10. Jahrgangsstufe
4
d) Beim Flug des Speers ist die Gewichtskraft die Zentripetalkraft, die den Speer auf der
Kreisbahn mit dem Radius rErde hält.
FG = FZp
mg = mv²/rErde

s
km 6370000m 7,9
s
m
v g r 9,81 Erde 2
    
e) Es sei r der Abstand eines Körpers der Masse m vom Erdmittelpunkt.
Dann gilt:
m g
2
1
r²
m M
G *
Erde   



9,01 10 m
9,81m/s²
2 6,67 10 m³/(kg s²) 5,974 10 kg
g
2 G* M
r
6
11 24
Erde  
    

 


Die Höhe über der Erdoberfläche berechnet sich damit zu:
h = r  rErde  2,64106 m = 2,64103
km
III. a) Longitudinalwelle: Schallwelle
Transversalwelle: Seilwelle, elektromagnetische Welle
b) c = f 
s
1 440
343m/s
f
c
    78 cm
c) Wellenmodell: Interferenz am Doppelspalt:
Überlagerung der von den beiden Spalten ausgehenden
Elementarwellen;
Konstruktive Interferenz bei Aufeinandertreffen von Wellenberg und Wellenberg, bzw. Wellental und Wellental;
Destruktive Interferenz bei Aufeinandertreffen von Wellenberg und Wellental;
Teilchenmodell: Fotoeffekt:
Die Photonen schlagen aus einer negativ geladenen Zinkplatte
Elektronen heraus. Dies geschieht nur, wenn die einzelnen
Photonen genügend Energie besitzen;