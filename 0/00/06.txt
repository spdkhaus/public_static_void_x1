Strings
■ String (engl.) = Zeichenkette 
■ Folge von Buchstaben und anderen Zeichen 
■ In Anführungszeichen geschrieben 
□ Einfache Anführungszeichen 'erste Variante'
□ Doppelte Anführungszeichen "zweite Variante"
Was ist ein String?
2
A B C 'a'
"Hallo"
":)"
'Simon mag Schokolade'
1 
2
Ausgabe von Strings
3
Stella
name = "Stella"
print(name)
print() 
■ Kann einen String übergeben bekommen 
■ Gibt nur den Teil zwischen den Anführungszeichen aus
1 
2 
3 
4
Operationen auf Strings
4
Hey Stella
anrede = "Hey"
name = "Stella"
ganze_anrede = anrede + " " + name
print(ganze_anrede)
Verketten von Strings
■ Mithilfe von + können Strings aneinander gehängt werden
■ String + String ➔ String
■ + erzeugt keine Leerzeichen, diese müssen selbst eingefügt werden
1 
2 
3
Operationen auf Strings
5
SimonSimonSimonSimon
name = "Simon"
vier_mal_simon = 4 * name 
print(vier_mal_simon)
Wiederholen von Strings 
■ Mithilfe von * können Strings mehrfach wiederholt werden 
■ Zahl * String ➔ String
1 
2 
3
Länge von Strings
6
16
name = "Simon und Stella"
zeichen_anzahl = len(name) 
print(zeichen_anzahl)
len() 
■ Funktion, die die Anzahl aller Zeichen eines Strings zurückgibt
Die Zukunft des Internets? | openHPI "50 Jahre Internet" | Prof. Dr. Christoph Meinel 
■ Platzhalter sind wie Lücken in Lückentexten 
■ Kann Lücken lassen und diese später ausfüllen 
"______ singt gerne."
Platzhalter
Stella
7
1 
2 
3
Platzhalter
8
Stella singt gerne.
person = "Stella"
satz = f"{person} singt gerne."
print(satz)
Platzhalter {variable} 
■ Lücke in einem String, die durch eine Variable ersetzt wird 
■ In die Klammern schreibt man den Variablennamen 
■ f oder F wird vor den String geschrieben 
■ Zeigt an, dass in dem String noch etwas ersetzt wird
1 
2 
3 
4
Platzhalter
9
Simon isst Kuchen, weil er Kuchen mag.
person = "Simon" 
speise = "Kuchen"
satz = f"{person} isst {speise}, weil er {speise} mag."
print(satz)
Platzhalter 
■ Man kann mehrere Platzhalter in einem String verwenden
1 
2 
3 
4
Platzhalter
9
Simon isst Kuchen, weil er Kuchen mag.
person = "Simon" 
speise = "Kuchen"
satz = f"{person} isst {speise}, weil er {speise} mag."
print(satz)
Platzhalter 
■ Man kann mehrere Platzhalter in einem String verwenden
person = "Simon" 
speise = "Kirschen"
satz = f"{person} isst {speise}, weil er {speise} mag."
print(satz)
Simon isst Kirschen, weil er Kirschen mag.
Die Zukunft des Internets? | openHPI "50 Jahre Internet" | Prof. Dr. Christoph Meinel 
■ Ein String ist eine Zeichenkette 
■ In Anführungszeichen (' oder ") geschrieben 
■ Kann Strings mit + verketten und mit * wiederholen 
■ len() bestimmt die Länge eines Strings 
■ {variable} dient als Platzhalter in f-Strings
Zusammenfassung
A B C
10
Viel Spaß 
bei den 
Aufgaben!