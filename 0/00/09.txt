Turtle (1/2)
■ Mechanische Schildkröte mit Stift 
■ Kann... 
□ ... sich vor und zurück bewegen 
□ ... zu den Seiten drehen 
□ ... ihren Stift anheben und wieder absetzen
Schildkrötengrafik
2
Ausgangsposition
3
(-200, -200) (200, -200)
(-200, 200) (200, 200)
(0, 0)
■ Turtle ist eine Bibliothek 
■ Bibliotheken stellen viele Funktionen bereit 
■ Bibliotheken kann man einbinden mit: 
from turtle import * 
from <bibliotheksname> import <funktionsnamen>
Turtle-Bibliothek
4
...
forward()
right()
1 
2
Funktion forward()
5
from turtle import *
forward(100)
forward() 
■ forward(schritte) - vorwärts laufen 
■ Schritte = Anzahl der Bildpunkte, die sich die Schildkröte bewegen 
soll
1 
2 
3 
4
Funktion right()
6
from turtle import *
forward(100) 
right(90) 
forward(100)
right() 
■ right(winkel) — nach rechts drehen 
■ Winkel = 0 bis 360 Grad, um die sich die Schildkröte drehen soll
1 
2
Funktion goto()
7
from turtle import *
goto(100,100)
goto() 
■ goto(x, y) — zu Punkt (x, y) laufen 
■ x, y = zwei Zahlen zwischen –200 und 200
(x, y)
1 
2 
3 
4 
5
Funktionen penup() und pendown()
8
from turtle import * 
penup() 
goto(0, -100) 
pendown() 
forward(100)
penup() und pendown() 
■ penup() — Stift anheben 
■ pendown() — Stift absetzten 
1 
2 
3
4
5
6
7
8
Ein Quadrat malen
9
from turtle import *
forward(100) 
right(90) 
forward(100) 
right(90) 
forward(100) 
right(90) 
forward(100) 
■ from turtle import * bindet die Turtle-Bibliothek mit ein 
■ Funktionen: 
□ forward(schritte) 
□ right(winkel) 
□ goto(x, y) 
□ penup() 
□ pendown()
Zusammenfassung
10
Viel Spaß 
bei den 
Aufgaben!