Die while-Schleife in Python
Neben der if-Verzweigung stellt die while-Schleife eine weitere – häufig 
genutzt – Kontrollstruktur zur Steuerung des Programmablaufs dar. Solange 
die Bedingung wahr (True) ist, wird die while-Schleife durchlaufen, wie 
folgendes Beispiel demonstriert:
i = 0
while i < 5:
 i = i + 1
 print(i)
1
2
3
4
5
Wie dem Beispiel entnommen werden kann, wird die Schleife mit dem 
Schlüsselwort while eingeleitet, gefolgt von einem booleschen Ausdruck und 
einem Doppelpunkt. Die Bedingung (bzw. der boolesche Ausdruck) lautet 
hier i < 5. Solange diese wahr ist, wird mit jedem Durchlauf zur Variablen i 
der Wert 1 addiert.
Wie bei der if-Verzweigung kann auch hier else verwendet werden:
i = 0
while i <= 10:
 i += 1
 print(i * 2)
else:
 print("i is now greater than 10")
Der Code im else-Block wird ausgeführt, sobald i <= 10 zu False evaluiert.
break-Anweisung
Des Weiteren kann die while-Schleife auch mit der if– und der breakAnweisung kombiniert werden.
i = 0
while i < 10:
 i = i + 1
 print(i)
 if i == 6:
 break
1
2
3
4
5
6
In diesem Fall werden nicht zehn Werte ausgegeben, sondern bei 6 endet die 
Ausgabe. Dies hängt damit zusammen, dass durch die Evaluierung von i ==
6 zu True die break-Anweisung ausgeführt wird, was die Beendigung der 
Schleife zur Folge hat.
continue-Anweisung
Neben break gibt es noch die continue-Anweisung. Betrachten wird dazu 
folgendes Beispiel:
value = 0
numbers = []
counter = 0
while counter < 3:
 value = input('Enter three even numbers: ')
 if int(value) % 2 != 0:
 print('The number must be even!')
 continue
 numbers.append(value)
 counter += 1
 print(f'Your numbers: {numbers}')
Der Nutzer wird dazu aufgefordert, drei gerade Zahlen einzugeben. Mithilfe 
einer if-Struktur wird überprüft, ob eine gerade Zahl eingegeben wurde. War 
dies nicht der Fall, dann greift die continue-Anweisung, die dazu führt, dass 
der aktuelle Schleifendurchlauf beendet wird.
Wurde hingegen eine gerade Zahl eingegeben, dann wird diese Zahl zur 
Liste numbers hinzugefügt und die Variabel counter wird um den Wert 1 
hochgezählt.
Der Unterschied zwischen break und continue besteht darin, dass 
durch break eine Schleife komplett abgebrochen wird, während 
bei continue nur der aktuelle Schleifendurchlauf beendet wird.
Die for-Schleife wird für Iterationen (lat.: iterare, wiederholen) eingesetzt. Mit 
anderen Worten: Damit kann man sequentielle Datentypen durchlaufen, sich 
also jedes einzelne Element solcher Typen ausgeben lassen. Häufig genutzte 
sequentielle Datentypen sind:
• Strings (Zeichenketten),
• Listen und
• Tupel.
Iteration sequentieller Datentypen
Im Falle eines Strings sieht das wie folgt aus:
programming_language = 'Python'
for i in programming_language:
 print(i)
P
y
t
h
o
n
Und eine Listen-Iteration sieht dementsprechend so aus:
towns = ['Dublin', 'Baku', 'Jakarta']
for i in towns:
 print(i)
Dublin
Baku
Jakarta
Und schließlich noch ein Beispiel zum Datentyp Tupel:
numbers = (22, 56, 19)
for i in numbers:
 print(i)
22
56
19
Ein Dictionary durchlaufen
Nicht fehlen darf selbstverständlich der wohl am häufigsten verwendete 
Datentyp: Dictionary. Auch hier kann eine for-Schleife genutzt werden, um 
sich zum Beispiel die Schlüssel-Wert-Paare anzeigen zu lassen:
soccer_scores = {'VfB Stuttgart': 45, 'FC St. Pauli': 30, 'SV Sandhausen': 29}
for key in soccer_scores:
 print(key, soccer_scores[key])
VfB Stuttgart 45
FC St. Pauli 30
SV Sandhausen 29
Auch wenn diese for-Schleife zum Ziel führt, also die Schlüssel mit den 
dazugehörigen Werten ausgegeben werden, gibt es 
einen besseren Weg: items()
soccer_scores = {'VfB Stuttgart': 45, 'FC St. Pauli': 30, 'SV Sandhausen': 29}
# Iteration mit items()
for key, val in soccer_scores.items():
 print(key, val)
VfB Stuttgart 45
FC St. Pauli 30
SV Sandhausen 29
Möchte man sich nur die Schlüssel, hier also die Fußballvereine ausgeben 
lassen, ginge dies mit folgendem Code:
for soccer_club in soccer_scores:
 print(soccer_club)
VfB Stuttgart
FC St. Pauli
SV Sandhausen
Die for-Schleife mit range() in 
Python
Schreibt man ein Programm, dann wird man häufig mit der Situation 
konfrontiert, dass etwas wiederholt werden muss. Möchte man beispielsweise 
viermal die Zeichenkette “Hello, World!” ausgeben, könnte man wie folgt 
vorgehen:
print("Hello, World!")
print("Hello, World!")
print("Hello, World!")
print("Hello, World!")
Damit kommt man sicherlich zum Ziel, denn bei Ausführung des Programms 
wird “Hello, World!” viermal angezeigt. Nichtsdestotrotz ist es umständlich.
Mit einer for-Schleife und der Built-In-Funktion range() läßt sich so etwas 
vereinfachen:
for i in range(4):
 print("Hello, World!")
Die print()-Funktion muss hier nur einmal geschrieben werden; den 
wiederholten Aufruf erledigt nun die for-Schleife. Bei einem viermaligen 
Aufruf mag das noch nicht so ins Gewicht fallen, aber bei Code, der 100 oder 
gar 1000 mal aufgerufen werden muss, sieht die Sache schon anders aus.
Dabei bezeichnet man die Zeile
for i in range(4):
als Schleifenkopf (loop header), während es sich bei dem dann folgenden –
eingerückten Code – um den Schleifenkörper (loop body) handelt. Es ist 
zwingend erforderlich, dass der Schleifenkörper eingerückt wird.
Jede Wiederholung des Codes im Schleifenkörper wird 
als Iteration bezeichnet. Da die Anzahl der Schleifendurchläufe hier 
angegeben wird, spricht man auch von einer bestimmten Iteration (definite 
iteration).
Mit der Iterationsvariablen i, deren Bezeichnung übrigens frei gewählt werden 
kann, werden die durchzuführenden Durchläufe gezählt. Dabei wird 
mit 0 begonnen und bis zum angegebenen Integer-Wert (hier: 4) minus 1 
hochgezählt.
for i in range(4):
 print(i)
 print("Hello, World!")
# Ausgabe:
0
Hello, World!
1
Hello, World!
2
Hello, World!
3
Hello, World!
Übrigens ist man durch die Angabe des Parameters 4 als Angabe der Anzahl 
der Iterationen unflexibel. Schöner ist es, eine Variable zu verwenden:
number = 4
for each_pass in range(number):
 print("Hello, World!")
Nutzt man range() mit zwei Parametern, dann wird dadurch ein Mindest- und 
ein Maximalwert festgelegt:
for i in range(4, 8):
 print(i)
# Ausgabe
4
5
6
7
Das funktioniert auch absteigend, wobei hier drei Parameter anzugeben sind:
for i in range(8, 4, -1):
 print(i)
# Ausgabe
8
7
6
5
hile-Schleife in Python
Über Schleifen können wir Aktion mehrmals ausführen lassen, bis eine 
festgelegte Bedingung erfüllt ist. So können wir z.B. in einem Shop 10 
Artikel ausgeben lassen. Die while-Schleife läuft 10-mal und gibt dann 10 
Artikel aus.
Wie sieht eine while-Schleife in Python aus? Wieder benötigen wir eine 
Bedingung, die wir bereits im Kapitel zu if-Bedingungen kennengelernt 
haben. Anhand dieser wird kontrolliert, ob die Schleife weiter „ihre Kreise“ 
ziehen darf – die Bedingung wird kontrolliert, ob diese noch richtig ist 
(sprich wahr ist). Ist die Bedingung nicht mehr wahr, wird die while-Schleife 
nicht mehr ausgeführt und das Programm läuft nach der Schleife weiter bis 
zum Ende des Programmcodes (wenn nach der Schleife noch Code 
kommt).
Im Vorfeld brauchen wir eine Variable, welche die Durchgänge zählt. Diese 
kontrollieren wir, ob diese kleiner 11 ist (damit wir 10 Durchgänge 
bekommen).
durchgang = 1
while durchgang < 11:
 print(durchgang)
print("nach der Schleife")
Wir sehen nun die Schleife und den typischen Aufbau mit der Einrückung. 
Alles was eingerückt wird, gehört in den Schleifenkörper und wird bei jedem 
Schleifendurchgang durchlaufen.
Allerdings läuft das obige Programm unendlich lang. Warum? Unsere 
Variable „durchgang“ ist immer unter 11, denn es ändert sich ja nichts an 
der Höhe der Zahl in der Variablen „durchgang“. Zum Abbrechen 
hilft ctrl + c
Daher müssen wir unbedingt im Schleifenkörper die Zahl bei jedem 
Durchgang um 1 erhöhen. Es muss also noch integriert werden durchgang = 
durchgang + 1. Und auch dieser Python-Code muss eingerückt in den 
Schleifenkörper, sonst wird dieser nicht als Bestand der Schleife 
ausgeführt!
durchgang = 1
while durchgang < 11:
 print(durchgang)
 durchgang = durchgang + 1
print("nach der Schleife")
Lassen wir nun unser Python-Programm ablaufen, erhalten wir folgende 
Ausgabe:
1
2
3
4
5
6
7
8
9
10
nach der Schleife
Hat die Variable dann den Wert 11, ist die Bedingung durchgang < 11 unwahr 
(false) und die Schleife wird verlassen und die Ausgabe „nach der Schleife“ 
erfolgt.
So einfach sind Schleifen umsetzbar. Angebracht ist, dass die Bedingung 
irgendwann auch „False“ annehmen kann, sonst läuft die Schleife unendlich 
lang (bis man das Kommando-Fenster schließt oder den Computer 
ausschaltet).
Schleifen
Allgemeiner Aufbau einer Schleife
Schleifen, werden benötigt, um einen Codeblock, den man auch als Schleifenkörper bezeichnet, 
wiederholt auszuführen. In Python gibt es zwei Schleifentypen: die while-Schleife und die for-Schleife.
Die meisten Schleifen enthalten einen Zähler oder ganz allgemein Variablen, die im Verlauf der 
Berechnungen innerhalb des Schleifenkörpers ihre Werte ändern. Außerhalb, d.h. noch vor dem 
Beginn der Schleife, werden diese Variablen initialisiert. Vor jedem Schleifendurchlauf wird geprüft, ob 
ein Ausdruck, in dem diese Variable oder Variablen vorkommen, wahr ist. Dieser Ausdruck bestimmt 
das Endekriterium der Schleife. Solange die Berechnung dieses Ausdrucks "True" liefert wird der 
Rumpf der Schleife ausgeführt. Nachdem alle Anweisungen des Schleifenkörpers durchgeführt 
worden sind, springt die Programmsteuerung automatisch zum Anfang der Schleife, also zur Prüfung 
des Endekriteriums zurück und prüft wieder, ob diese nochmals erfüllt ist.
Wenn ja, geht es wie oben beschrieben weiter, ansonsten wird der Schleifenkörper nicht mehr 
ausgeführt und es wird mit dem Rest des Skriptes fortgefahren. Das nebenstehende Diagramm zeigt 
dies schematisch.
Einfaches Beispiel einer Schleife
Wir möchten dies nun an einem kleinen Python-Skript verdeutlichen, welches die Summe der Zahlen 
von 1 bis 100 bildet. Im nächsten Kapitel über for-Schleifen werden wir eine weitaus elegantere 
Möglichkeiten zu diesem Zweck kennenlernen.
n = 100
s = 0
i = 1
while i <= n:
 s = s + i
 i = i + 1
print "Die Summe lautet: ", s 
Standard-Eingabe lesen
Bevor wir mit der while-Schleife weitermachen, müssen wir noch ein paar grundsätzliche Dinge über 
die Standardeingabe und die Standardausgabe klären. Als Standardeingabe gilt normalerweise die 
Tastatur. Die meisten Shell-Programme schreiben ihre Ausgaben in die Standardausgabe, d.h. das 
Terminalfenster oder die Textkonsole. Fehlermeldungen werden in die Standard-Fehlerausgabe 
ausgegeben, was üblicherweise auch dem aktuellen Terminalfenster oder der Textkonsole entspricht.
Auch der Python-Interpreter stellt drei Standard-Dateiobjekte zur Verfügung:
• Standardeingabe
• Standardausgabe
• Standardfehlerausgabe
Sie stehen im Modul sys als
• sys.stdin
• sys.stdout
• sys.stderror
zur Verfügung.
Das folgende Beispiel-Skript zeigt nun, wie man Zeichen für Zeichen mittels einer while-Schleife von 
der Standardeingabe (Tastatur) einliest. Mit dem import-Befehl wird das benötigte Modul sys 
eingelesen.
import sys 
text = ""
while 1:
 c = sys.stdin.read(1)
 text = text + c
 if c == '\n':
 break
print "Eingabe: %s" % text
Eleganter kann man eine beliebige Eingabezeile von der Standardeingabe natürlich mit der Funktion 
raw_input(prompt) einlesen.
>>> name = raw_input("Wie heißen Sie?\n")
Wie heißen Sie?
Tux
>>> print name
Tux
>>> 
Der else-Teil
Wie auch die bedingte if-Anweisung hat die while-Schleife in Python im Gegensatz zu anderen 
Programmiersprachen einen optionalen else-Zweig, was für viele Programmierer 
gewöhnungsbedürftig ist.
Die Anweisungen im else-Teil werden ausgeführt, sobald die Bedingung nicht mehr erfüllt ist. 
Sicherlich fragen sich einige nun, worin dann der Unterschied zu einer normalen while-Schleife liegt. 
Hätte man die Anweisungen nicht in den else-Teil gesteckt sondern einfach hinter die while-Schleife 
gestellt, wären sie ja auch genauso ausgeführt worden. Es wird erst mit einem break-Kommando, was 
wir später kennenlernen sinnvoll.
Allgemein sieht eine while-Schleife mit else-Teil in Python wie folgt aus:
while Bedingung:
Anweisung1
...
Anweisungn
else:
Anweisung1
...
Anweisungn
Vorzeitiger Abbruch einer while-Schleife
Normalerweise wird eine Schleife nur beendet, wenn die Bedingung im Schleifenkopf nicht mehr 
erfüllt ist. Mit break kann man aber eine Schleife vorzeitig verlassen und mit continue einen Durchlauf 
beenden.
Im folgenden Beispiel, einem einfachen Zahlenratespiel, kann mam erkennen, dass in Kombination 
mit einem break der else-Zweig durchaus sinnvoll sein kann. Nur wenn die while-Schleife regulär 
beendet wird, d.h. der Spieler die Zahl erraten hat, gibt es einen Glückwunsch. Gibt der Spieler auf, 
d.h. break, dann wird der else-Zweig der while-Schleife nicht ausgeführt.
import random
n = 20
to_be_guessed = int(n * random.random()) + 1
guess = 0
while guess != to_be_guessed:
 guess = input("Neue Zahl: ")
 if guess > 0:
 if guess > to_be_guessed:
 print "Zahl zu groß"
 elif guess < to_be_guessed:
 print "Zahl zu klein"
 else:
 print "Schade, Sie geben also auf!"
 break
else:
 print "Glückwunsch! Das war's!"
for-Schleifen
Syntax der For-Schleife
for Variable in Sequenz:
Anweisung1
Anweisung2
...
Anweisungn
else:
Else-Anweisung1
Else-Anweisung2
...
Else-Anweisungm
Die for-Anweisung hat einen unterschiedlichen Charakter zu den for-Schleifen, die man aus den 
meisten anderen Programmiersprachen kennt. In Python dient die for-Schleife zur Iteration über ein 
Sequenz von Objekten, während sie in anderen Sprachen meist nur "eine etwas andere whileSchleife" ist.
Beispiel einer for-Schleife in Python:
>>> languages = ["C", "C++", "Perl", "Python"] 
>>> for x in languages:
... print x
... 
C
C++
Perl
Python
>>> 
Die range()-Funktion
Mit Hilfe der range()-Funktion lässt sich die for-Schleife ideal für Iterationen nutzen. range() liefert 
Listen, die arithmetischen Aufzählungen entsprechen.
Beispiel:
>>> range(10)
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Obiges Beispiel zeigt, dass Range mit einem Argument aufgerufen die Liste der Zahlen von 0 bis zu 
diesem Argument liefert.
range() kann aber auch mit zwei Argumenten aufgerufen werden:
range(begin,end)
Dann wird eine Liste aller ganzen Zahlen von begin (einschließlich) bis end (aussschließlich) geliefert.
Als drittes Argument kann man range() noch die Schrittweite mitgeben.
Beispiel:
>>> range(4,10)
[4, 5, 6, 7, 8, 9]
>>> range(4,50,5)
[4, 9, 14, 19, 24, 29, 34, 39, 44, 49]
Besonders sinnvoll wird die range()-Funktion im Zusammenspiel mit der for-Schleife. Im 
nachfolgenden Beispiel bilden wir die Summe der Zahlen von 1 bis 100:
n = 100
s = 0
for i in range(1, n+1):
 s = s + i
print s
In obigem kleinen Programm verbirgt sich aber noch ein schreckliches Effizienzproblem. Was 
geschieht bevor die for-Schleife ausgeführt wird? Python wertet zuerst den Aufruf range(1, n+1) aus. 
Das bedeutet, dass eine Liste mit 100 Zahlen erzeugt wird, also [1, 2, 3, 4, ... 100]. Es werden zwar 
alle Zahlen dieser Liste innerhalb der Schleife benötigt, aber zu keinem Zeitpunkt die ganze Liste. Im 
vorigen Kapitel hatte wir dieses Problem mit einer while-Schleife gelöst und dort benötigten wir auch 
keine Liste. Python bietet eine Lösung für dieses Problem, indem es die Funktion xrange zur 
Verfügung stellt. xrange erzeugt ein iterierbares Objekt (iterable), das bedeutet, dass keine Liste 
erzeugt wird sondern zum Beispiel in einer for-Schleife über die Werte iteriert werden kann ohne dass 
die Liste erzeugt wird:
>>> for i in xrange(1, 7):
... print(i)
... 
1
2
3
4
5
6
>>> 
Obige Schleife verhält sich im Hinblick auf die Effizienz ähnlich wie folgende while-Schleife:
>>> i = 1
>>> while i < 7:
... print(i)
... i += 1
... 
1
2
3
4
5
6
>>> 
Im Ausgabeverhalten sieht man natürlich keinen Unterschied. Den Unterschied zwischen range und 
xrange sieht man aber, wenn man die Aufrufe direkt in der interaktiven Python-Shell tätigt:
>>> range(1,7)
[1, 2, 3, 4, 5, 6]
>>> xrange(1,7)
xrange(1, 7)
>>> 
Beispiel: Berechnung der pythagoräischen Zahlen
Die meisten glauben, dass der Satz von Pythagoras von Pythagoras entdeckt worden war. 
Warum sonst sollte der Satz seinen Namen erhalten haben. Aber es gibt eine Debatte, ob dieser Satz 
nicht auch unabhängig von Pyhtagoras und vor allen Dingen bereits früher entdeckt worden sein 
könnte. Für die Pythagoräer - eine mystische Bewegung, die sich auf die Mathematik, Religion und die 
Philosophie begründete - waren die ganzen Zahlen, die den Satz des Pythagoras erfüllten besondere 
Zahlen, die für sie heilig waren.
Heutzutage haben die Pythagoräischen Zahlen nichts mystisches mehr. Obwohl sie für manche 
Schülerin oder Schüler oder ander Personen, die mit der Mathematik auf Kriegsfuß stehen, immer 
noch so erscheinen mögen.
Ganz unromantisch gilt in der Mathematik:
Drei natürliche Zahlen, welche die Gleichung a2+b2=c2 erfüllen, heißen pythagoräische Zahlen.
Das folgende Programm berechnet alle pythagoräischen Zahlen bis zu einer einzugebenden 
maximalen Zahl:
#!/usr/bin/env python
from math import sqrt
n = raw_input("Maximale Zahl? ")
n = int(n)+1
for a in xrange(1, n):
 for b in xrange(a, n):
 c_square = a**2 + b**2
 c = int(sqrt(c_square))
 if ((c_square - c**2) == 0):
 print a, b, c
Iteration über Liste mit range()
Falls man auf die Indexe einer Liste zugreifen möchte, scheint es keine gute Idee zu sein eine ForSchleife zur Iteration über die Liste zu nutzen. Man kann dann zwar alle Elemente erreichen, aber der 
Index eines Elementes ist nicht verfügbar. Aber es gibt eine Möglichkeit sowohl auf den Index als auch 
auf das Element zugreifen zu können. Die Lösung besteht darin range() in Kombination mit der len()-
Funktion, die einem die Anzahl der Listenelemente liefert, zu benutzen:
fibonacci = [0,1,1,2,3,5,8,13,21]
for i in xrange(len(fibonacci)):
 print i,fibonacci[i]
print
Listen-Iteration mit Seiteneffekten
Falls man über eine Liste iteriert, sollte man vermeiden die Liste im Schleifenkörper (body) zu 
verändern. Was passieren kann, zeigen wir im folgenden Beispiel:
colours = ["red"]
for i in colours:
 if i == "red":
 colours += ["black"]
 if i == "black":
 colours += ["white"]
print colours
Was wird durch die Anweisung "print colours" ausgegeben?
['red', 'black', 'white']
Am besten benutzt man eine Kopie der Liste, wie im nächsten Beispiel:
colours = ["red"]
for i in colours[:]:
 if i == "red":
 colours += ["black"]
 if i == "black":
 colours += ["white"]
print colours
Die Ausgabe sieht nun wie folgt aus:
['red', 'black']
Auch jetzt haben wir die Liste verändert, aber "bewusst" innerhalb des Schleifenkörpers. Aber der 
Elemente, die über die For-Schleife interiert werden, bleiben unverändert durch die Iterationen.