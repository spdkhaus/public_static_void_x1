Variablen
kleidung
Was sind Variablen?
2
kleidung kleidung
"T-Shirt"
"Hose"
kleidung = "T-Shirt" 
kleidung = "Hose"
Variablen anlegen
3
1 
2 
Variablen 
■ Erlauben den Zugriff auf Werte über einen Namen 
■ Zuweisung: Variable = Wert
Variablenname Wert
kleidung
"T-Shirt"
kleidung = "T-Shirt" 
kleidung = "Hose"
Variablen anlegen
4
1 
2 
Variablen 
■ Erlauben den Zugriff auf Werte über einen Namen 
■ Zuweisung: Variable = Wert
■ Variablenwerte können sich ändern
kleidung
"Hose"
1 
2 print(kleidung)
Variablen verwenden
4
T-Shirt
1 "T-Shirt")
1 
2 print(kleidung)
Variablen verwenden
4
kleidung = "T-Shirt"
Variablen verwenden 
■ Wenn wir print() den Namen einer Variablen übergeben, wird nicht 
dieser Name, sondern der Wert in der Variable ausgegeben
T-Shirt
1 
2 
3
1 
2
T-Shirt
kleidung = "T-Shirt"
zweite_kleidung = kleidung 
print(zweite_kleidung) 
NameError
print(dritte_kleidung) 
5
Variablen verwenden
!
zweiter_name 
ZWEITER_NAME 
Zweiter_Name}
■ variablen_werden_klein_geschrieben_und_mit_unterstrich_getrennt 
■ Keine Umlaute (ä, ö, ü) oder Sonderzeichen (wie !)
Variablen benennen
6
verschiedene Variablen
■ Groß- und Kleinschreibung ist entscheidend
■ Variablen sollten aussagekräftig benannt werden
■ Erlauben den Zugriff auf Werte über einen Namen 
■ Variablen werden mit variablenname = wert angelegt 
□ kleidung = "T-Shirt"
■ Änderung der Werte jederzeit möglich
Zusammenfassung
7
Viel Spaß 
bei den 
Aufgaben!