Mit Hilfe der Print-Funktion kann man in Python Dinge ausgeben. Sie funktioniert so: print(„Hello World“). 

Print heißt drucken auf Deutsch und gibt aus, was man davor in Klammern und evtl. Anführungszeichen geschrieben hat. 

Jede Print-Funktion erzeugt eine eigene Zeile bei der Ausgabe. 

Wenn das, was man ausgeben möchte keine Variable ist, dann wird die Ausgabe in Anführungszeichen geschrieben. 
Wenn es Variablen sind, wird die Ausgabe ohne Anführungszeichen geschrieben. 

 

 

Kommentare sind Texte im Code, die nicht ausgeführt werden. 

Sie funktionieren so: #Das ist ein Kommentar 

Oder so: “““Das ist ein Kommentar“““ 