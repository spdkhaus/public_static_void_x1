# public_static_void_x1

*"Wie cultivire ich die Freiheit bei dem Zwange? Ich soll meinen Zögling gewöhnen, einen Zwang seiner Freiheit zu dulden, und ich soll ihn selbst zugleich anführen, seine Freiheit gut zu gebrauchen. Ohne dies ist alles bloßer Mechanism, und der der Erziehung Entlassene weiß sich seiner Freiheit nicht zu bedienen." Immanuel Kant, Über Pädagogik*


* https://open.hpi.de/courses/pythonjunior-schule2024
* https://web.powerva.microsoft.com/
* https://prezi.com/view/dwwBFG05ZNnoqr8c25nE/

[AW23]  Albrecht, C.; Wampfler, P.: Zugänge zu einer zeitgemäßen Prüfungskultur. In: Führer, C.;
        Magirius, M.; Bohl, T.; Grewe, B.-S.; Polleichtner, W.; Ulfat, F.(Hrsg): Relativität und
        Normativität von Beurteilungen. Fachübergreifen unde fachspezifische Analysen, Schriftenreihe
        2 der Tübingen School of Education, Tübingen Library Publishing, Tübingen, 2023, S. 45-52.

[Ka03]  Kant, I.: Über Pädagogik. 1803.

[NM21]  Nölte, B.; Wampfler, P.: Eine Schule ohne Noten. Neue Wege zum Umgang mit Lernen und 
        Leistung,  hep-Verlag, Bern, 2021.

[Wi11]  Winter, F.: Leistungsbewertung. Eine neue Lernkultur braucht einen anderen Umgang mit 
        Schülerleistungen, Grundlagen der Schulpädagogik (Band 49), 4. unveränderte Auflage, Schneider Verlag Hochgehren, Baltmannsweiler, 2011.